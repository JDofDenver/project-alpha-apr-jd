Thank you for taking the time to look at my first project from theHackReactor course that I attended.

This is a to do list app made with Django, HTML, and CSS as a Monolith application.

To make things easier, I've dockerized the project so you don't have to set up any environments.
To run the project follow the commands below

Make sure you're in the root of the project in your terminal (this will have the Dockerfile on the first level)

Run the following command to create your images:
docker build -t project-alpha-apr-jd .

Once your container is running, open the terminal and run:
python manage.py migrate

Once your images are made, run this command to create and start your containers:
docker run -p 8000:8000 project-alpha-apr-jd

You should be able to access the project at localhost:8000 in your browser
